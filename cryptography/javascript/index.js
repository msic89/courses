
const text = document.getElementById('text');
const cryptor = document.getElementById('cryptor');
const result = document.getElementById('result');
const title = document.getElementById('title');
const alpha = document.getElementById('alpha');


function crypt() {
    if (text.value != '') {
        switch (cryptor.value) {
            case "0":
                result.innerHTML = encodeCesar(text.value)
                break;
            case "1":
                result.innerHTML = encodeMonoAlphabet(text.value)
                break;
            case "2":
                result.innerHTML = encodeWiener(text.value, 'keyke')
                break;
            default:
                result.innerHTML = md5(text.value)
                break;
        }
    } else {
        result.innerHTML = ""
    }
}

text.addEventListener('keyup', function (e) {
    crypt();
});
cryptor.addEventListener('change', function (e) {
    // title.innerText = e.target.innerText
    crypt();
});

alpha.innerHTML = getShannonWienerTable().map(function (aaa) {
    return aaa.map(function (a) {
        return `<span>${a}</span>`;
    }).join('')
}).join('')

