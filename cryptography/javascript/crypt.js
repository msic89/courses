const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
const alphabet2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
const characters = ["'", '"', '`', '~', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '=', '+', '/', '\'', '|', ';', ':', '.', '?', '>', '<', ' ']



function encodeMonoAlphabet(text) {
    const tab = [
        ['638', '696', '562', '449', '251', '612', '980', '251', '554', '576', '310', '929', '616', '729', '607', '551', '789', '646', '580', '667', '442', '280', '594', '434', '394', '852'],
        ["789", "251", "807", "339", "248", "315", "927", "819", "137", "577", "946", "074", "052", "542", "237", "256", "598", "523", "094", "079", "072", "616", "654", "196", "563", "813"]
    ]
    const encoded = Array.from(text).map(function (c) {
        const tb = isUpperCase(c) ? alphabet2 : alphabet;
        const pas = tb.indexOf(c);
        return isUpperCase(c) ? tab[1][pas] : tab[0][pas];
    })
    return encoded.join('');
}

function encodeCesar(text, k = 3) {
    const encoded = Array.from(text).map(function (c) {
        const tab = isUpperCase(c) ? alphabet2 : alphabet;
        const pas = tab.indexOf(c) + k;
        const pivot = pas > 25 ? pas - 25 : pas;
        return tab[pivot];
    })
    return encoded.join('');
}

function encodeWiener(text, key) {
    const wiener = getShannonWienerTable();

    const encoded = Array.from(text).map(function (c, index) {
        const i = alphabet2.indexOf(c.toUpperCase());
        const j = alphabet2.indexOf(key[index].toUpperCase());
        return wiener[j][i];
    });

    return encoded.join('');
}

function md5(text) {
    return CryptoJS.MD5(text);
}



/* helpers */

// dependencies
function randomArray(max = 26) {
    const t = [];
    for (let i = 0; i < max; i++) {
        const a = rand() + "" + rand() + "" + rand();
        t.push(a);
    }
    return t;
}

function getDoubleValues(array) {
    const t = [];
    for (let i = 0; i < array.length; i++) {
        for (let j = i + 1; j < array.length; j++) {
            if (array[i] === array[j]) {
                t.push(array[i])
            }
        }
    }
    return t;
}
function rand(max = 10) {
    return Math.floor(Math.random() * max);
}

function isUpperCase(text) {
    if (text == text.toUpperCase()) {
        return true
    }
    return false
}

function getAllCharacters() {
    const chars = [];
    for (var i = 32; i <= 126; i++) {
        chars.push(String.fromCharCode(i));
    }
    return chars;
}

function getShannonWienerTable() {
    const ta = []
    for (let i = 0; i < 26; i++) {
        const tb = []
        for (let j = i; j < 26 + i; j++) {
            const a = j > 25 ? j - 26 : j
            tb.push(alphabet2[a])
        }
        ta.push(tb)
    }
    return ta
}

